/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.proyecto.ejemplociclo3.controladores;

import com.proyecto.ejemplociclo3.interfaces.ProductoService;
import com.proyecto.ejemplociclo3.modelo.Producto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author SILVIA MORENO
 */




//Se trabaja con el nuevo software
@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ProductoController {
     

     @Autowired
     private ProductoService productoService;

     @PostMapping(value = "/")
     public ResponseEntity<Producto> agregar (@RequestBody Producto producto){
          Producto obj= productoService.save(producto);
          System.out.println("producto add "+obj.getNombreProducto());
           System.out.println("EJEMPLOOOOOOOO");
          return new ResponseEntity<>(obj,HttpStatus.OK);
     }
     
     @PostMapping(value = "/p")
     public ResponseEntity<Producto> agregar2 (@RequestBody Producto producto){
          Producto obj= productoService.save(producto);
          System.out.println("producto add "+obj.getNombreProducto());
              System.out.println("PRUEBAAAAA");
          return new ResponseEntity<>(obj,HttpStatus.OK);
     }

     @DeleteMapping(value="/list/{id}")
     public ResponseEntity<Producto> eliminar (@PathVariable Integer id){
          Producto obj = productoService.findById(id);
          if(obj !=null){
               productoService.delete(id);
          }
          else {
               return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
          }
          return new ResponseEntity<>(obj,HttpStatus.OK);
     }

     @PutMapping(value = "/list/{id}")
     public ResponseEntity<Producto> editar(@RequestBody Producto producto){
          Producto obj = productoService.findById(producto.getIdProducto());
               if(obj!=null){
                    obj.setCantidad(producto.getCantidad());
                    obj.setNombreProducto(producto.getNombreProducto());
                    obj.setValorCompra(producto.getValorCompra());
                    obj.setValorVenta(producto.getValorVenta());
               productoService.save(obj);
               } else {
                    return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
               }
          return new ResponseEntity<>(obj,HttpStatus.OK);
     }

     @GetMapping(value="/list")
     public List<Producto> consultarTodo(){
          return productoService.findAll();
     }
     


     @GetMapping(value="/list/{id}")
     public Producto consultaPorId(@PathVariable Integer id){
          return productoService.findById(id);
     }

}
