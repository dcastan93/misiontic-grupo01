/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.proyecto.ejemplociclo3.servicios;

import com.proyecto.ejemplociclo3.interfaces.DetalleService; 
import com.proyecto.ejemplociclo3.modelo.Detalle;
import com.proyecto.ejemplociclo3.servicios.repositorios.DetalleDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author SILVIA MORENO
 */
@Service
public class DetalleServiceImpl implements DetalleService{

          @Autowired
          private DetalleDAO detalleDAO;

          @Override
          @Transactional(readOnly=false)
          public Detalle save(Detalle detalle){
                return detalleDAO.save(detalle);
          }  

          @Override
          @Transactional(readOnly=false)
          public void delete(Integer id){
               detalleDAO.deleteById(id);
          }

          @Override
          @Transactional(readOnly=true)
          public Detalle findById(Integer id){
               return  detalleDAO.findById(id).orElse(null);
          }

          @Override
          @Transactional(readOnly=true)
          public List<Detalle> findAll(){
               return (List<Detalle>) detalleDAO.findAll();
          }

}
 