/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.proyecto.ejemplociclo3.servicios.repositorios;

import com.proyecto.ejemplociclo3.modelo.Detalle;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author SILVIA MORENO
 */
public interface DetalleDAO extends CrudRepository<Detalle, Integer>{
     
}
