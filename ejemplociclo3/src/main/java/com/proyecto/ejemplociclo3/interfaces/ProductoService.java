/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.proyecto.ejemplociclo3.interfaces;

import com.proyecto.ejemplociclo3.modelo.Producto;
import java.util.List;

/**
 *
 * @author SILVIA MORENO
 */
public interface ProductoService {
     
      public Producto save(Producto producto);
      public void delete(Integer id);
      public Producto findById(Integer id);
      public List<Producto> findAll();

}
